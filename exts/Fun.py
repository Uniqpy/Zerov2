import discord
from discord.ext import commands
import asyncio

class Fun:
    '''Commands for Fun'''
    
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def ping(self, ctx):
        """Returns bot's latency."""
        ping_time = int(self.bot.latency*1000)
        await ctx.send(f"**🏓 Pong! Took {ping_time}ms.**")
    
    @commands.command()
    async def say(self, ctx, *, content: commands.clean_content=None):
        '''Makes bot say something.'''
        if not content:
            await ctx.send('\U0001f612')
            return
        await asyncio.sleep(0.5)
        await ctx.message.delete()
        await ctx.send(content)

def setup(bot):
    bot.add_cog(Fun(bot))
