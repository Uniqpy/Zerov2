import discord
import datetime
import asyncio
from discord.ext import commands

class mod:
    '''
    Commands for Moderation
    '''
    def __init__(self, bot):
        self.bot = bot
    
    @commands.has_permissions(kick_members=True)
    @commands.command()
    async def kick(self, ctx, member: discord.Member, *, reason: str=None):
        '''
        Kicks someone from your guild.
        '''
        if reason is not None:
            auditreason = f'kicked by {ctx.message.author}, {reason}'
        else:
            auditreason = f'kicked by {ctx.message.author}, No reason provided.'
        await member.kick(reason=auditreason)
        e = discord.Embed(color=discord.Color.gold(), timestamp=datetime.datetime.utcnow(), description=f"__**Reason**__ : {reason}")
        e.set_author(name=f'{ctx.message.author} kicked {member}', icon_url=ctx.message.author.avatar_url or 'http://www.solidbackgrounds.com/images/950x350/950x350-red-solid-color-background.jpg')
        e.set_footer(text=ctx.message.author)
        await ctx.send(embed=e)
    
    @commands.has_permissions(ban_members=True)
    @commands.command()
    async def ban(self, ctx, member: discord.Member, *, reason: str=None):
        '''
        Bans someone from your guild.
        '''
        if reason is not None:
            auditreason = f'banned by {ctx.message.author}, {reason}'
        else:
            auditreason = f'banned by {ctx.message.author}, No reason provided.'
        await member.ban(reason=auditreason)
        e = discord.Embed(color=discord.Color.gold(), timestamp=datetime.datetime.utcnow(), description=f"__**Reason**__ : {reason}")
        e.set_author(name=f'{ctx.message.author} banned {member}', icon_url=ctx.message.author.avatar_url or 'http://www.solidbackgrounds.com/images/950x350/950x350-red-solid-color-background.jpg')
        e.set_footer(text=ctx.message.author)
        await ctx.send(embed=e)

def setup(bot):
    bot.add_cog(mod(bot))
