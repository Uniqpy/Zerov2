import os

import discord
from discord.ext import commands

from botclass import Zero

bot = Zero(case_insensitive=True)

class Main():
    def __init__(self, bot):
        self.bot = bot

    async def on_ready(self):
        print('Logged in as')
        print(self.bot.user.name)
        for i in bot.exts:
            self.bot.load_extension(f'exts.{i}')

    async def on_message_edit(self, before, after):
        await self.bot.process_commands(after)

bot.add_cog(Main(bot))
bot.run(os.getenv('TOKEN'))
# test comment

